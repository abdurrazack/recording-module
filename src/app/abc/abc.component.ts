import { Component, OnInit, NgZone } from '@angular/core';
import '../../../recorder.js';

@Component({
  selector: 'app-abc',
  templateUrl: './abc.component.html',
  styleUrls: ['./abc.component.css']
})
export class AbcComponent implements OnInit {
  private recorder;
  private recStream
  private mediaRecorder;
  private deviceslist;
  private selectedDevice;
  private Abps;
  private analyserNode;
  constructor(private ngzone: NgZone) { }

  ngOnInit() {
    this.initRec();
  }

  startRec() {
    var recorder = document.getElementById('recorder');
    this.mediaRecorder = new MediaRecorder(this.recStream);
    this.mediaRecorder.start();
    console.log(this.mediaRecorder.state);
    console.log("recorder started");
    this.Abps = this.mediaRecorder.audioBitsPerSecond;
  }
  blobToFile(theBlob: Blob, fileName: string): File {
    var b: any = theBlob;
    b.lastModifiedDate = new Date();
    b.name = fileName;
    return <File>theBlob;
  }
  stopRec() {
    var chunks = [];
    var audio = document.createElement('audio');
    this.mediaRecorder.ondataavailable = (e) => {
      chunks.push(e.data);
      var filelink = this.blobToFile(e.data, "thedata.ogg");
      var aElement = document.createElement("a");
      document.body.appendChild(aElement);
      var blob = new Blob(chunks, { 'type': 'audio/ogg; codecs=opus' });
      chunks = [];
      var audioURL = URL.createObjectURL(blob);
      audio.src = audioURL;
      audio.controls = true;
      document.body.appendChild(audio);
    }
    this.mediaRecorder.stop();
    console.log("recorder stopped");
  }
  handleSuccess(stream) {
    var context = new AudioContext();
    var source = context.createMediaStreamSource(stream);
    var processor = context.createScriptProcessor(1024, 1, 1);
    var analyser = context.createAnalyser();
    analyser.smoothingTimeConstant = 0.8;
    analyser.fftSize = 1024;
    source.connect(analyser);
    analyser.connect(processor);

    source.connect(processor);
    processor.connect(context.destination);
    this.showConsole(context.createAnalyser(), source, processor);
    processor.onaudioprocess = (e) => this.ngzone.run(() => {
      // Do something with the data, i.e Convert this to WAV
      //console.log(e.inputBuffer);
      var array = new Uint8Array(analyser.frequencyBinCount);
      analyser.getByteFrequencyData(array);
      let tot = array.reduce(function (acc, val) { return acc + val; });
      this.analyserNode = tot / array.length;
      console.log(this.analyserNode);

    });

  }
  showConsole(a, b, c) {
    console.log(a);
    console.log(b);
    console.log(c);
  }
  initRec() {
    navigator.mediaDevices.getUserMedia({ audio: true })
      .then((stream) => {
        this.handleSuccess(stream);
        console.log(stream);
        this.recStream = stream;
        console.log("Initialized");
        this.getRecSource();
      })
  }
  getRecSource() {
    navigator.mediaDevices.enumerateDevices().then((devices) => {
      devices = devices.filter((d) => d.kind === "audioinput");
      this.deviceslist = devices;
      console.log(devices);
    }).catch();
  }
  changeRecSource() {
    navigator.mediaDevices.getUserMedia({
      audio: {
        deviceId: this.selectedDevice
      }
    });

  }

}
